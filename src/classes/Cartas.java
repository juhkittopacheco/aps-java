/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author HP
 */
public class Cartas {
    
  public static Carta[] cartas;
  
  static
  {
     cartas = new Carta[10];
     cartas[0] = new Carta("Plantio-de-arvores.jpg","plantar árvores","<html><h1><u>Plantar árvores</u></h1>\n" +
            "<h3>Plante uma árvore!<br/> \n" +
            "É fácil, e além de deixar sua cidade mais bonita ajuda o meio ambiente.<br/>\n" +
            "As árvores ajudam a diminuir a temperatura, purificam o ar, servem de abrigos para os animais,\n" +
            "evitam o desgaste do solo e ainda servem de sombra além de deixam o ambiente fresquinho.</h3></html>");     
     cartas[1] = new Carta("lixo_na_rua.jpg","lixo na rua","<html><h1><u>Lixo nas ruas</u></h1>" +
            "<h3> Além de sujar a cidade quem joga lixo nas ruas ainda contribui " +
            "para o entupimento das redes de esgoto, o que pode gerar enchentes em épocas de chuva!" +
            "Não importa o tamanho do lixo, sempre procure uma lixeira para descartar!<br/> " +
            "Ás vezes um pequeno papel descartado no local certo pode fazer toda a diferença.</h3></html>");     
     cartas[2] = new Carta("poluicao-da-agua.jpg","floresta","<html><h1><u>Lixo marinho</u></h1>\n" +
            "<h3>O lixo marinho é todo detrito que é jogado propositalmente ou cai por acidente nos mares e oceanos,\n" +
            "contribuindo para o aumento da poluição marinha.<br/> \n" +
            "Este lixo é responsável por diversos problemas ambientais na atualidade.\n" +
            "E grande parte do lixo marinho é composta por materiais plásticos.<br/>\n" +
            "Aproximadamente 6.4 milhões de toneladas de lixo são descartadas nos oceanos e mares a cada ano. Mais de 13.000 pedaços \n" +
            "de lixo plástico estão, atualmente, flutuando em cada quilômetro quadrado de oceano.</h3></html>");
     cartas[3] = new Carta("lixo_animais_marinhos.jpg","floresta","<html><h1><u>Consumo de lixo por animais marinhos</u></h1>\n" +
            "<h3>Diversos animais marinhos são diariamente vítimas das alterações das condições das águas ou \n" +
            "pelo consumo de lixo, já que muitos animais marinhos ingerem lixo por confundir com alimentos.\n" +
            "Como plásticos e os metais, demoram muito tempo para serem dissolvidos, são os maiores causadores\n" +
            "de mortes, lesões e ferimentos de espécies marinhas.\n" +
            "O crescimento constante do uso de plástico e o baixíssimo índice mundial de reciclagem do material,\n" +
            "apenas contribuem para o aumento do problema.</h3></html>");
     cartas[4] = new Carta("queimadas.jpg","floresta","<html><h1><u>Queimadas</u></h1>\n" +
            "<h3> As queimadas provocam a alteração o equilíbrio dos ecossistemas das mais diversas paisagens, impactando diretamente na vida das plantas e animais da região,\n" +
            "na circulação de águas superficiais e subterrâneas, nas condições de temperatura e umidade,\n" +
            "e na liberação de vapor de água na atmosfera.<br/>\n" +
            "Além de acabar com o ambiente onde ocorre, as queimadas colocam em risco a vida dos animais que\n" +
            "vivem na região e nos arredores.</h3></html>");
     cartas[5] = new Carta("Plantio-de-arvores.jpg","floresta","<html><h1><u>Plantar árvores</u></h1>\n" +
            "<h3>Plante uma árvore!<br/> \n" +
            "É fácil, e além de deixar sua cidade mais bonita ajuda o meio ambiente.<br/>\n" +
            "As árvores ajudam a diminuir a temperatura, purificam o ar, servem de abrigos para os animais,\n" +
            "evitam o desgaste do solo e ainda servem de sombra além de deixam o ambiente fresquinho.</h3></html>");     
     cartas[6] = new Carta("lixo_na_rua.jpg","floresta","<html><h1><u>Lixo nas ruas</u></h1>" +
            "<h3> Além de sujar a cidade quem joga lixo nas ruas ainda contribui " +
            "para o entupimento das redes de esgoto, o que pode gerar enchentes em épocas de chuva! <br/>" +
            "Não importa o tamanho do lixo, sempre procure uma lixeira para descartar!<br/> " +
            "Ás vezes um pequeno papel descartado no local certo pode fazer toda a diferença.</h3></html>");
     cartas[7] = new Carta("poluicao-da-agua.jpg","floresta","<html><h1><u>Lixo marinho</u></h1>\n" +
            "<h3>O lixo marinho é todo detrito que é jogado propositalmente ou cai por acidente nos mares e oceanos,\n" +
            "contribuindo para o aumento da poluição marinha.<br/> \n" +
            "Este lixo é responsável por diversos problemas ambientais na atualidade.\n" +
            "E grande parte do lixo marinho é composta por materiais plásticos.<br/>\n" +
            "Aproximadamente 6.4 milhões de toneladas de lixo são descartadas nos oceanos e mares a cada ano. Mais de 13.000 pedaços \n" +
            "de lixo plástico estão, atualmente, flutuando em cada quilômetro quadrado de oceano.</h3></html>");
     cartas[8] = new Carta("lixo_animais_marinhos.jpg","floresta","<html><h1><u>Consumo de lixo por animais marinhos</u></h1>\n" +
            "<h3>Diversos animais marinhos são diariamente vítimas das alterações das condições das águas ou \n" +
            "pelo consumo de lixo, já que muitos animais marinhos ingerem lixo por confundir com alimentos.\n" +
            "Como plásticos e os metais, demoram muito tempo para serem dissolvidos, são os maiores causadores\n" +
            "de mortes, lesões e ferimentos de espécies marinhas.\n" +
            "O crescimento constante do uso de plástico e o baixíssimo índice mundial de reciclagem do material,\n" +
            "apenas contribuem para o aumento do problema.</h3></html>");
     cartas[9] = new Carta("queimadas.jpg","floresta","<html><h1><u>Queimadas</u></h1>\n" +
            "<h3> As queimadas provocam a alteração o equilíbrio dos ecossistemas das mais diversas paisagens, impactando diretamente na vida das plantas e animais da região,\n" +
            "na circulação de águas superficiais e subterrâneas, nas condições de temperatura e umidade,\n" +
            "e na liberação de vapor de água na atmosfera.<br/>\n" +
            "Além de acabar com o ambiente onde ocorre, as queimadas colocam em risco a vida dos animais que\n" +
            "vivem na região e nos arredores.</h3></html>");     
     
     


     //fazer o resto quando tiver as demais imagens
  }
    
}
