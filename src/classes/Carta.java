/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author HP
 */
public class Carta {
    private String link;
    private String nomeCarta;
    private String informacao;
    
      public String getLink()    
    {
        return  link;     
    }
      
    public void setLink(String link)
    {
        this.link = link;
    }
    
      public String getNomeCarta()
    {
        return nomeCarta;
    }
    
    public void setNomeCarta(String nomeCarta)
    {
        this.nomeCarta = nomeCarta;
    }
    
   public String getInformacao()
    {
        return informacao;
    }
    
    public void setInformacao(String informacao)
    {
        this.informacao = informacao;
    }
    
 
     //método construtor
    public Carta(String link, String nomeCarta, String informacao)
    {
        //era para ser "this.objeto;" apenas
        this.setLink(link);
        this.setNomeCarta(nomeCarta);
        this.setInformacao(informacao);
    }
    }
