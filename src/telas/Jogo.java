/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telas;

import classes.Carta;
import classes.Cartas;
import static classes.Cartas.cartas;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Random;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Joao
 */
public final class Jogo extends javax.swing.JFrame {

    public static Carta[] todasCartasArrumadas;
    public static Carta[] cartasEscolhidas = new Carta[2];    
    public static JLabel[] cartasEscolhidasJLabel =  new JLabel[2];    
    public static int liberado = 0, acertos = 0, contCartasSelecionadas = 0, codCartaRodadaAnterior = -1;



    
    /**
     * Creates new form Jogo
     * @param novaCarta
     */
    public Carta[] embaralhaCartas(Carta[] todasCartas){
        
        Random random = new Random();;
		
        for (int i=0; i < (todasCartas.length); i++) {

                //sorteia um índice
                int j = random.nextInt(todasCartas.length); 

                //troca o conteúdo dos índices i e j do vetor
                Carta temp = todasCartas[i];
                todasCartas[i] = todasCartas[j];
                todasCartas[j] = temp;
        }
        
        return todasCartas;
    }
    
    public void escondeAsCartas(){
        imagem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));        
        imagem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));        
        imagem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        imagem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        imagem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        imagem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));        
        imagem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));        
        imagem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        imagem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        imagem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
    }
    public void escondeAsCartasUnica(JLabel imagem){
        imagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png")));
        Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
        imagem.setBorder(border);
    }
    
    public void criaCartas(Carta[] todasCartas){
        
        
        todasCartas = embaralhaCartas(todasCartas);
        int contCartasSelecionadas = 0;
        System.out.println(todasCartas[0].getLink());
        imagem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[0].getLink())));        
        imagem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[1].getLink())));        
        imagem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[2].getLink())));
        imagem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[3].getLink())));
        imagem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[4].getLink())));
        imagem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[5].getLink())));        
        imagem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[6].getLink())));        
        imagem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[7].getLink())));
        imagem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[8].getLink())));
        imagem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartas[9].getLink())));
        
        todasCartasArrumadas = todasCartas;

        imagem1.addMouseListener(escolheCarta(imagem1,0));
        imagem2.addMouseListener(escolheCarta(imagem2,1));
        imagem3.addMouseListener(escolheCarta(imagem3,2));
        imagem4.addMouseListener(escolheCarta(imagem4,3));
        imagem5.addMouseListener(escolheCarta(imagem5,4));
        imagem6.addMouseListener(escolheCarta(imagem6,5));
        imagem7.addMouseListener(escolheCarta(imagem7,6));
        imagem8.addMouseListener(escolheCarta(imagem8,7));
        imagem9.addMouseListener(escolheCarta(imagem9,8));
        imagem10.addMouseListener(escolheCarta(imagem10,9));
        
        
    }
    
    public MouseAdapter escolheCarta(final JLabel imagem, final int CodImagem){
        final JFrame _self = this;
        return new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if ((liberado != 0)&&(codCartaRodadaAnterior != CodImagem)) {
                    if (contCartasSelecionadas  <= 1) {
                        Border border = BorderFactory.createLineBorder(Color.GREEN, 3);
                        imagem.setBorder(border);
                        imagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/"+todasCartasArrumadas[CodImagem].getLink())));        
                        cartasEscolhidas[contCartasSelecionadas] =  todasCartasArrumadas[CodImagem];
                        cartasEscolhidasJLabel[contCartasSelecionadas] = imagem;
                        contCartasSelecionadas++;
                        codCartaRodadaAnterior = CodImagem;
                    }


                    if ((contCartasSelecionadas  == 2)&&(cartasEscolhidas[0].getLink().equals(cartasEscolhidas[1].getLink()))) {    
                        acertos++;
                        new Informacao(cartasEscolhidas[0], acertos, _self).setVisible(true);
                        System.out.println(cartasEscolhidas[0].getInformacao());
                        contCartasSelecionadas = 0;    
                        cartasEscolhidas = new Carta[2];
                    }
                    else if(contCartasSelecionadas  == 2) {
                        JOptionPane.showMessageDialog(null, "Errou, Tente novamente :D");
                        contCartasSelecionadas = 0;   
                        cartasEscolhidas = new Carta[2];
                        escondeAsCartasUnica(cartasEscolhidasJLabel[0]);
                        escondeAsCartasUnica(cartasEscolhidasJLabel[1]);
                        cartasEscolhidasJLabel = new JLabel[2];

                    }
                }
            }
        };
    }
    
    
    public class principal extends javax.swing.JFrame {
                                           

        public void recebe(String mensagem){

            lblpreparar.setText(mensagem);//labelTempo não está apresentando a mensagem

        }

    }

    
    public class Tempo implements Runnable{
    
        public void run(){

            int min=0,seg=9;
            principal j= new principal();

            try{

                j.recebe(""+seg);
                System.out.println("0"+min+":"+seg);

//                for(min=1; min>=0; min--){

                   for(seg=9; seg>=0; seg--){

                       Thread.sleep(1000);
                       j.recebe(""+seg);
                       System.out.println("0"+min+":"+seg);
                       if(seg==0){
                           j.recebe("");
                           escondeAsCartas();
                           liberado = 1;
                       }
                   }


//                }

            }catch(InterruptedException e){
                e.printStackTrace();
            }
    
        }
    }
    


    public Jogo() {
        initComponents(); 
    }
   
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCancelarOuComecar = new javax.swing.JButton();
        imagem4 = new javax.swing.JLabel();
        imagem1 = new javax.swing.JLabel();
        imagem2 = new javax.swing.JLabel();
        imagem3 = new javax.swing.JLabel();
        imagem5 = new javax.swing.JLabel();
        imagem10 = new javax.swing.JLabel();
        imagem6 = new javax.swing.JLabel();
        imagem9 = new javax.swing.JLabel();
        imagem7 = new javax.swing.JLabel();
        imagem8 = new javax.swing.JLabel();
        lblpreparar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setName("frJogar"); // NOI18N
        setResizable(false);

        btnCancelarOuComecar.setBackground(new java.awt.Color(46, 125, 50));
        btnCancelarOuComecar.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N
        btnCancelarOuComecar.setForeground(new java.awt.Color(27, 94, 32));
        btnCancelarOuComecar.setText("começar");
        btnCancelarOuComecar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 7, 1));
        btnCancelarOuComecar.setBorderPainted(false);
        btnCancelarOuComecar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelarOuComecar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarOuComecarActionPerformed(evt);
            }
        });

        imagem4.setBackground(new java.awt.Color(0, 0, 0));
        imagem4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem4.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem4.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem4.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem1.setBackground(new java.awt.Color(255, 255, 255));
        imagem1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem1.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem1.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem1.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem2.setBackground(new java.awt.Color(0, 0, 0));
        imagem2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem2.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem2.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem2.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem3.setBackground(new java.awt.Color(0, 0, 0));
        imagem3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem3.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem3.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem3.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem5.setBackground(new java.awt.Color(0, 0, 0));
        imagem5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem5.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem5.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem5.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem10.setBackground(new java.awt.Color(255, 255, 255));
        imagem10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem10.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem10.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem10.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem6.setBackground(new java.awt.Color(255, 255, 255));
        imagem6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem6.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem6.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem6.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem9.setBackground(new java.awt.Color(255, 255, 255));
        imagem9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem9.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem9.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem9.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem7.setBackground(new java.awt.Color(255, 255, 255));
        imagem7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem7.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem7.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem7.setPreferredSize(new java.awt.Dimension(193, 273));

        imagem8.setBackground(new java.awt.Color(255, 255, 255));
        imagem8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imagem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/refresh.png"))); // NOI18N
        imagem8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        imagem8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        imagem8.setMaximumSize(new java.awt.Dimension(193, 273));
        imagem8.setMinimumSize(new java.awt.Dimension(193, 273));
        imagem8.setPreferredSize(new java.awt.Dimension(193, 273));

        lblpreparar.setFont(new java.awt.Font("Comic Sans MS", 0, 36)); // NOI18N
        lblpreparar.setForeground(new java.awt.Color(27, 94, 32));
        lblpreparar.setText("10");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(imagem6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(imagem1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(imagem2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(imagem7, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem8, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem9, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imagem10, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblpreparar)
                        .addGap(424, 424, 424)
                        .addComponent(btnCancelarOuComecar, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(imagem4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imagem3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imagem2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imagem1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imagem5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(imagem10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(imagem9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(imagem8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(imagem7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(imagem6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblpreparar)
                    .addComponent(btnCancelarOuComecar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarOuComecarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarOuComecarActionPerformed

        if (btnCancelarOuComecar.getText() == "começar") {
            criaCartas(Cartas.cartas);  
            Tempo j= new Tempo();
            Thread th= new Thread(j);
            th.start();
            btnCancelarOuComecar.setText("cancelar");
        }
        else{
            setVisible(false);
        }
    }//GEN-LAST:event_btnCancelarOuComecarActionPerformed
   
    
 

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                JFrame newGame = new Jogo();
                newGame.setVisible(true);
                            
            }
        });         
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelarOuComecar;
    private javax.swing.JLabel imagem1;
    private javax.swing.JLabel imagem10;
    private javax.swing.JLabel imagem2;
    private javax.swing.JLabel imagem3;
    private javax.swing.JLabel imagem4;
    private javax.swing.JLabel imagem5;
    private javax.swing.JLabel imagem6;
    private javax.swing.JLabel imagem7;
    private javax.swing.JLabel imagem8;
    private javax.swing.JLabel imagem9;
    private javax.swing.JLabel lblpreparar;
    // End of variables declaration//GEN-END:variables
}
